-- creature

INSERT INTO `creature` VALUES (280010464, 6000545, 1904, 9553, 9553, '12', 0, 0, 0, -1, 0, 0, -8645.1, 772.892, 45.399, 1.76948, 300, -1, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, '', 0);
INSERT INTO `creature` VALUES (280010457, 6000540, 1904, 9553, 9675, '12', 0, 0, 0, -1, 0, 0, -8665.99, 918.107, 89.734, 5.6214, 300, -1, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, '', 0);
INSERT INTO `creature` VALUES (280010454, 6000501, 1904, 0, 0, '12', 0, 0, 0, -1, 0, 0, -8290.86, 1348.96, 5.55557, 1.58179, 300, -1, 0, 0, 9136, 0, 0, 0, 0, 0, 0, 0, '', 0);

-- creature_template

INSERT INTO `creature_template` VALUES (6000540, 0, 0, 0, 0, 0, '.', NULL, '', NULL, 'questinteract', 6000541, 1, 1, 0, 0, 0, 35, 1, 1, 1.14286, 1, 0, 0, 0, 0, 1, 1, 1, 768, 2048, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 2, '', 28153);
INSERT INTO `creature_template` VALUES (6000545, 0, 0, 0, 0, 0, '.', NULL, '', NULL, 'questinteract', 6000546, 1, 1, 0, 0, 0, 35, 1, 1, 1.14286, 1, 0, 0, 0, 0, 1, 1, 1, 768, 2048, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 2, '', 28153);

-- creature_template_model

INSERT INTO `creature_template_model` VALUES (6000540, 0, 86528, 1, 1, 28153);
INSERT INTO `creature_template_model` VALUES (6000545, 0, 86528, 1, 1, 28153);

-- gossip_menu

INSERT INTO `gossip_menu` VALUES (6000541, 6000542, 29704);
INSERT INTO `gossip_menu` VALUES (6000546, 6000547, 29704);

-- creature_template_scaling

INSERT INTO `creature_template_scaling` VALUES (6000501, 0, 110, 120, 0, 0, 0, 28768);

-- gossip_menu_option

INSERT INTO `gossip_menu_option` VALUES (6000541, 0, 2, 'Entrer dans la Prison', 6000542, 1, 1, 33369);
INSERT INTO `gossip_menu_option` VALUES (6000546, 0, 2, 'Sortir de la Prison', 6000547, 1, 1, 33369);

-- npc_text

INSERT INTO `npc_text` VALUES (6000540, 1, 1, 1, 0, 0, 0, 0, 0, 6000541, 0, 0, 0, 0, 0, 0, 0, 28768);
INSERT INTO `npc_text` VALUES (6000545, 1, 1, 1, 0, 0, 0, 0, 0, 6000546, 0, 0, 0, 0, 0, 0, 0, 28768);

-- quest_objectives

INSERT INTO `quest_objectives` VALUES (337138, 50769, 3, 0, 0, 6000545, 1, 1, 0, 0, 'Sortir de la prison', 28153);
INSERT INTO `quest_objectives` VALUES (337140, 50769, 3, 0, 0, 6000501, 1, 1, 0, 0, 'Parler à Nathanos le Flétrisseur', 28153);

-- quest_poi

INSERT INTO `quest_poi` VALUES (50769, 0, 6, 2, 337138, 6000545, 1, 85, 0, 2, 0, 0, 1478764, 0, 30706);
INSERT INTO `quest_poi` VALUES (50769, 0, 8, 2, 337140, 6000501, 1, 85, 0, 2, 0, 0, 1478764, 0, 30706);
INSERT INTO `quest_poi` VALUES (50769, 0, 7, 32, 0, 0, 1, 85, 0, 0, 0, 0, 1478244, 0, 30706);
INSERT INTO `quest_poi` VALUES (50769, 0, 9, 32, 0, 0, 1, 85, 0, 0, 0, 0, 1478244, 0, 30706);

-- quest_poi_points

INSERT INTO `quest_poi_points` VALUES (50769, 6, 0, -8645, 773, 30706);
INSERT INTO `quest_poi_points` VALUES (50769, 7, 0, -8291, 1364, 30706);
INSERT INTO `quest_poi_points` VALUES (50769, 8, 0, -8290, 1348, 30706);
INSERT INTO `quest_poi_points` VALUES (50769, 9, 0, 62, 57, 30706);

-- smart_scripts

INSERT INTO `smart_scripts` VALUES (6000540, 0, 0, 0, 62, 0, 100, 0, 6000541, 0, 0, 0, 0, '', 62, 1904, 0, 0, 0, 0, 0, 7, 0, 0, 0, -8718.74, 1005.87, 45.3991, 3.72434, 'Accès aux égouts de la Prison');
INSERT INTO `smart_scripts` VALUES (6000545, 0, 0, 0, 62, 0, 100, 0, 6000546, 0, 0, 0, 0, '', 62, 1904, 0, 0, 0, 0, 0, 7, 0, 0, 0, -8291.03, 1364.5, 5.56366, 4.71998, 'Sortie de la prison');
